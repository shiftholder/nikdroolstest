package unused_lookupstuffhere;

import org.drools.core.SessionConfiguration;
import org.drools.core.common.EventFactHandle;
import org.drools.core.time.SessionPseudoClock;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.model.KieBaseModel;
import org.kie.api.builder.model.KieModuleModel;
import org.kie.api.builder.model.KieSessionModel;
import org.kie.api.cdi.KBase;
import org.kie.api.conf.EqualityBehaviorOption;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.definition.type.FactType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.api.runtime.conf.ClockTypeOption;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.internal.marshalling.MarshallerFactory;

import java.util.Date;
import java.util.concurrent.TimeUnit;


public class KBaseManager {
    /*
    static KieServices kieServices;
    static KieModuleModel kieModuleModel;
    static KieFileSystem kfs;
    static KieBuilder kieBuilder;
    static boolean success = false;

    public unused_lookupstuffhere.KBaseManager() {


        kieServices = KieServices.Factory.get();
        kieModuleModel = kieServices.newKieModuleModel();
        //kieModuleModel.setConfigurationProperty("","");
        kfs = kieServices.newKieFileSystem();

        KieBaseModel kieBaseModel1 = kieModuleModel.newKieBaseModel( "KBase1 ")
                .setDefault( true )
                .setEqualsBehavior( EqualityBehaviorOption.EQUALITY )
                .setEventProcessingMode( EventProcessingOption.STREAM );

        //kieBaseModel1.addPackage("com.customSyntax.thing");

        KieSessionModel ksessionModel1 = kieBaseModel1.newKieSessionModel( "KSession1" )
                .setDefault( true ) //DOES NOT WORK
                .setType( KieSessionModel.KieSessionType.STATEFUL )
                .setClockType( ClockTypeOption.get("pseudo") );
                //.setClockType( ClockTypeOption.get("realtime") );

        kfs.write( "src/Main/resources/KBase1/Sample.drl",
                kieServices.getResources().newFileSystemResource("customResources/Sample.drl"));
        //byte[] test = kfs.read("src/Main/resources/KBase1/Sample.drl");

        kfs.writeKModuleXML(kieModuleModel.toXML());
        //String test = kieModuleModel.toXML();
        kieBuilder = kieServices.newKieBuilder( kfs );
        kieBuilder.buildAll();

        try{
            kieBuilder.getKieModule();
        }
        catch(Throwable t){
            success = false;
            return;
        }
        success = true;
    }

    public static String GetBuildMesssages(){
        String buildmsg = "";
        buildmsg += GetSuccess() ? "BUILD SUCCEEDED\n\n" : "BUILD FAILED\n\n";
        buildmsg += "BUILD MESSAGES:\n\n";
        for(var m : kieBuilder.getResults().getMessages())
            buildmsg += m.getText() + "\n";
        return buildmsg;
    }

    public static boolean GetSuccess(){
        return success;
    }

    public static KieContainer buildKieContainer(){
        return  kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
    }
    */
}
