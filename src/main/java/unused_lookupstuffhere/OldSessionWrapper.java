package unused_lookupstuffhere;

import org.kie.api.KieBase;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.builder.model.KieBaseModel;
import org.kie.api.builder.model.KieModuleModel;
import org.kie.api.builder.model.KieSessionModel;
import org.kie.api.conf.EqualityBehaviorOption;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.api.runtime.conf.ClockTypeOption;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class OldSessionWrapper {


    private boolean runnable = false;

    public boolean isRunnable() {
        return runnable;
    }

    private List<String> configurationSources;
    private KieServices kieServices;
    private KieFileSystem kieFileSystem;
    private KieBuilder kieBuilder;
    private KieModuleModel kieModuleModel;
    //private KieBaseModel kieBaseModel;
    //private KieSessionModel kieSessionModel;
    private KieBaseConfiguration kieBaseConfiguration;
    private KieSessionConfiguration kieSessionConfiguration;
    private KieModule kieModule;
    private KieContainer kieContainer;
    private KieBase kieBase;
    private KieSession kieSession;

    public KieSession getKieSession() {
        return kieSession;
    }

    public OldSessionWrapper(List<String> configurationSources) {
        this.configurationSources = configurationSources;
    }

    public boolean Build() {
        kieServices = KieServices.Factory.get();
        kieModuleModel = kieServices.newKieModuleModel();
        //kieModuleModel.setConfigurationProperty("","");
        kieFileSystem = kieServices.newKieFileSystem();

        //kieBaseModel = kieModuleModel.newKieBaseModel( "KBase");
                //.setDefault( true )
                //.setEqualsBehavior( EqualityBehaviorOption.EQUALITY )
                //.setEventProcessingMode( EventProcessingOption.STREAM );

        //kieBaseModel1.addPackage("com.customSyntax.thing");

        //kieSessionModel = kieBaseModel.newKieSessionModel( "KSession" );
                //.setDefault( true ) //DOES NOT WORK
                //.setType( KieSessionModel.KieSessionType.STATEFUL )
                //.setClockType( ClockTypeOption.get("pseudo") );
        //.setClockType( ClockTypeOption.get("realtime") );

        CreateKieFileSystemFromConfiguration();

        kieFileSystem.writeKModuleXML(kieModuleModel.toXML());
        //String test = kieModuleModel.toXML();
        kieBuilder = kieServices.newKieBuilder( kieFileSystem );
        kieBuilder.buildAll();

        try{
            kieModule = kieBuilder.getKieModule();
        }
        catch(Throwable t){
            runnable = false;
            return false;
        }


        // get container
        // TODO: check if this works with release id
        var test = kieModule.getReleaseId();
        kieContainer = kieServices.newKieContainer(kieModule.getReleaseId());

        kieBase = kieContainer.newKieBase(kieBaseConfiguration);
        kieSession = kieBase.newKieSession(kieSessionConfiguration, null );

        // get clock from session
        //sessionPseudoClock = kSession.getSessionClock();
        // make a marshaller
        //marshaller = new unused_lookupstuffhere.Marshaller(kBase);

        runnable = true;
        return true;
    }

    private void CreateKieFileSystemFromConfiguration(){
        for (String rel :  configurationSources) {
            Path path = Paths.get(rel);
            //if(path.endsWith("/*") || path.endsWith("/"))
            //if(path.endsWith(".txt"))
            // "customResources/Sample.drl"
            kieFileSystem.write( "src/Main/resources/KBase/" + path.getFileName().toString(),
                    kieServices.getResources().newFileSystemResource(rel));
        }
    }




}
