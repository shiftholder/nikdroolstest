package unused_lookupstuffhere;

import org.kie.api.KieBase;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.internal.marshalling.MarshallerFactory;

import java.io.*;

public class Marshaller {

    org.kie.api.marshalling.Marshaller marshaller;
    File file;
    KieSessionConfiguration kieSessionConfiguration;

    public Marshaller(KieBase kieBase) throws IOException {
        marshaller = MarshallerFactory.newMarshaller(kieBase);
        file = new File("./testFiles/test.msl");
        file.createNewFile();
    }

    public void Marshall(KieSession kSession) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        kieSessionConfiguration = kSession.getSessionConfiguration();
        marshaller.marshall(fos, kSession);
    }

    public KieSession Unmarshall() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(file);
        return marshaller.unmarshall(fis, kieSessionConfiguration, null);
    }
}
