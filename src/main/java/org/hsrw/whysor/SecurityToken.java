package org.hsrw.whysor;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SecurityToken {
    public static String OnCallMethod(String username, String password) throws InterruptedException, ExecutionException, TimeoutException {
        //CONFIG PARAMETERS:
        //BEGIN------------CONFIG PARAMETERS BELOW TO YOUR ENVIRONMENT---------------------------------------
        String baseURL = "https://api.dev.whysor.com/users/login";
        final String body= String.format("{\"email\": \"%s\", \"password\": \"%s\", \"domain\": \"my.dev.yookr.org\"}", username, password);

        var client = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .followRedirects(HttpClient.Redirect.ALWAYS)
                .build();
        HttpRequest request = null;

        try {
            request = HttpRequest.newBuilder(new URI(baseURL))
                    .POST(HttpRequest.BodyPublishers.ofString(body))
                    .setHeader("Content-Type", "application/json")
                    .setHeader("Accept", "application/json")
                    .header("Authority","https://api.dev.whysor.com/")
                    .build();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
//            client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
//                .thenApply(HttpResponse::body)
//                .thenAccept(System.out::println)
//                .join();

        CompletableFuture<HttpResponse<String>> response = client.sendAsync(request, HttpResponse.BodyHandlers.ofString());
        String result = response.thenApply(HttpResponse::body).get(5, TimeUnit.SECONDS);
        return result;

    }


}





