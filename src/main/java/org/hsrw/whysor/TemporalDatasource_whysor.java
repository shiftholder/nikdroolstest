package org.hsrw.whysor;

import org.json.JSONObject;
import org.junit.Test;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class TemporalDatasource_whysor {

    @Test
    public void testFetch(){

        try {
            ReadUserConfig();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        String outputDevices;
        //String extension = "devices";
        String extension = "methods";
        try {
            outputDevices = FetchDataFromApi(extension);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
            return;
        }

    }


    private static String path = "./testFiles/conf/whysor/";
    private static String token = "0";
    private static String oldId = "1";
    private static String username;
    private static String password;

    public static void ReadUserConfig() throws IOException {
        FileReader fileReader = new FileReader(path + "userConf.txt");
        Properties properties = new Properties();
        properties.load(fileReader);

        username = properties.getProperty("user");
        password = properties.getProperty("password");

    }

    public static String FetchDataFromApi(String extension) throws InterruptedException, ExecutionException, TimeoutException {

        String baseURL = "https://api.dev.whysor.com/";
        String outputToken;
        BufferedWriter bw = null;

        if (token.equals(oldId)) {
            //do nothing
        } else {
//            SecurityToken securityToken = new SecurityToken();
            try {
                outputToken = SecurityToken.OnCallMethod(username, password);
                JSONObject obj = new JSONObject(outputToken);
                token = obj.getString("id");

                File file = new File(path + "securityToken.txt");
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileWriter fw = new FileWriter(file);
                bw = new BufferedWriter(fw);
                bw.write(token);
                bw.flush();
                bw.close();
                oldId = token;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (TimeoutException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (bw != null)
                        bw.close();
                } catch (Exception ex) {
                    System.out.println("Error in closing the BufferedWriter" + ex);
                }
            }
        }


        var client = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .followRedirects(HttpClient.Redirect.ALWAYS)
                .build();
        HttpRequest request = null;

        try {

            request = HttpRequest.newBuilder(new URI(baseURL + extension + "?access_token=" + token))
//                    .POST(HttpRequest.BodyPublishers.ofString(extension+"?access_token="+token))
                    .setHeader("Authorization", token)
                    .setHeader("access_token", token)
                    .setHeader("Content-Type", "application/json")
                    .setHeader("Accept", "application/json")
                    .build();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        CompletableFuture<HttpResponse<String>> response = client.sendAsync(request, HttpResponse.BodyHandlers.ofString());
        String result = response.thenApply(HttpResponse::body).get(5, TimeUnit.SECONDS);

        File file2 = new File(path + extension + "Output.json");
        if (!file2.exists()) {
            try {
                file2.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        FileWriter fw = null;
        try {
            fw = new FileWriter(file2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        bw = new BufferedWriter(fw);

        try {
            bw.write(result);
            bw.flush();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return result;
    }



}
