
import KieSimplifier.SessionWrapper;
import KieSimplifier.TimespanHelper;
import com.customSyntax.Place;
import com.customSyntax.TestDataProvider_sensor;
import com.customSyntax.TestDataProvider_weather;
import org.drools.core.common.EventFactHandle;
import org.drools.core.time.SessionPseudoClock;
import org.kie.api.KieBase;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.conf.EqualityBehaviorOption;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.definition.type.FactType;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.api.runtime.conf.ClockTypeOption;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.api.runtime.rule.QueryResults;

import javax.swing.*;
import java.io.File;
import java.time.Instant;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class Main {

    static SessionWrapper sw;

    static KieServices kieServices;
    static KieContainer kContainer;
    static KieBase kBase;
    static KieSession kSession;
    static SessionPseudoClock sessionPseudoClock;

    static ArrayList<ObjectifiedKieSession> debugKieSessions = new ArrayList<>();
    static ChartDisplay chartDisplay;
    static ezui ui;
    public static void makeui()
    {
        JFrame frame = new JFrame("facts");
        ui = new ezui();
        frame.setContentPane(ui.myPane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        // this is bullshit
        ui.fuckthisshit();
    }



    public static void main(String[] args) {
        makeui();
        chartDisplay = new ChartDisplay(debugKieSessions);
        ui.chartDisplay = chartDisplay;
        String firstPageMessage = "";
        // get service
        kieServices = KieServices.Factory.get();
        /*
        try {
            // info string for the first page of the UI


            // make base manager
            unused_lookupstuffhere.KBaseManager kbm = new unused_lookupstuffhere.KBaseManager();
            // this fetches eventual error messages (and other info?)
            firstPageMessage += kbm.GetBuildMesssages();


            if(kbm.GetSuccess()){
                // get container
                kContainer = kbm.buildKieContainer();
                // make base
                kBase = kContainer.newKieBase(kieServices.newKieBaseConfiguration());
                // make session
                kSession = kBase.newKieSession(kContainer.getKieSessionConfiguration("KSession1"), null );
                // get clock from session
                sessionPseudoClock = kSession.getSessionClock();
                // make a marshaller
                marshaller = new unused_lookupstuffhere.Marshaller(kBase);
                // add info to first page
                firstPageMessage += "\n\n" + GetKieInfo();
            }

            ui.AddString(firstPageMessage);

            if(kbm.GetSuccess())
                Simulate();

        } catch (Throwable t) {
            t.printStackTrace();
        }
        */

        // config
        KieBaseConfiguration kbc = kieServices.newKieBaseConfiguration();
        kbc.setOption(EventProcessingOption.STREAM);
        kbc.setOption(EqualityBehaviorOption.EQUALITY);
        KieSessionConfiguration ksc = kieServices.newKieSessionConfiguration();
        ksc.setOption(ClockTypeOption.get("pseudo"));

        // resources
        var resources = new ArrayList<Resource>();
        resources.add(kieServices.getResources().newFileSystemResource(new File("customResources/Sample.drl")));

        // release id
        ReleaseId rid = kieServices.newReleaseId("test01", "artID", "v1");

        try{
            sw = new SessionWrapper( rid, kbc, ksc, resources );
        } catch (RuntimeException e){
            firstPageMessage += e.toString();
            ui.AddString(firstPageMessage);
            return;
        }

        try {
            sw.setStorageWrapper(new StorageWrapper_File("./testFiles/sessions/"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        kBase = sw.getKieBase();
        kSession = sw.getKieSession();
        sessionPseudoClock = kSession.getSessionClock();

        sessionPseudoClock.advanceTime(Instant.now().getEpochSecond() - sessionPseudoClock.getCurrentTime(), TimeUnit.MILLISECONDS);

        firstPageMessage += sw.getKieInfo();
        ui.AddString(firstPageMessage);

        Simulate();
    }


    static void ShowAllFacts(){
        ui.AddString(sw.getAllFactsAsString());
    }


    // run something
    static void Simulate() {

        // insert some facts
        final int numPlaces = 5;
        final int numThings = 4;

        TestDataProvider_sensor tds = new TestDataProvider_sensor();
        TestDataProvider_weather tdw = new TestDataProvider_weather();
        sw.addTemporalDatasource(tds);
        sw.addTemporalDatasource(tdw);

        for(int i = 0; i < numPlaces; i++){
            Place p = new Place(i,i);
            tds.places.add(p);
            kSession.insert(p);
        }

        kSession.insert(new TimespanHelper(0,30000,"react to weather"));

        //for(int i = 0; i < numThings; i++)
        //    InsertThing("T" + i);

        sw.run(
                sessionPseudoClock.getCurrentTime() + 1000 * 1000,
                null,
                sessionWrapper -> {
                    // TODO: replace with using debugKieSessions
                    ShowAllFacts();
                    debugKieSessions.add(new ObjectifiedKieSession(kSession));
                    return true;
                }
            );

        // query test
        String thingName = "T0";
        long timeAgo = 35000; // in ms
        long timeStamp = sessionPseudoClock.getCurrentTime() - timeAgo;
        //System.out.println(" query test: " + thingName + " was at " + GetWhatWasWhere(thingName, timeStamp) + timeAgo + "ms ago, when time was " + timeStamp );

        // see if any sessions were duplicated for some reason
        ui.AddString(sw.getKieInfo() + "\n\nruntime = " + sw.getRunTimeTaken() + "\nrunCount = " + sw.getRunCounter());
    }


    // TEST WORKING MEMORY INTERACTIONS:

    // make a event of an in-drl declared type and insert it
    static EventFactHandle InsertTestEvent(String thing, int x, int y) {
        FactType factType = kBase.getFactType("com.sample.streamBase", "thingMoved");
        Object factInstance = null;
        try {
            factInstance = factType.newInstance();

            //factType.set(factInstance, "name", name);
            factType.set(factInstance, "timeStamp", sessionPseudoClock.getCurrentTime());
            Object t = GetThing(thing);
            if(t != null)
                factType.set(factInstance, "t", t);
            Object p = GetPlace(x,y);
            if(p != null)
                factType.set(factInstance, "p", p);
            return (EventFactHandle) kSession.insert(factInstance);

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    static Object GetPlace(int x, int y){
        QueryResults queryResults = kSession.getQueryResults("getPlace", x, y);
        if(queryResults.size() > 0){
            return queryResults.iterator().next().get("$p");
        }
        else{
            System.out.println("Place " + x + " " + y + " not found");
            return null;
        }
    }

    static Object GetThing(String name){
        QueryResults queryResults = kSession.getQueryResults("getThing", name);
        if(queryResults.size() > 0){
            return queryResults.iterator().next().get("$t");
        }
        else{
            System.out.println("thing " + name + " not found");
            return null;
        }
    }

    // takes thing name, returns Place
    static Object GetWhatWasWhere(String name, long TS){
        QueryResults queryResults = kSession.getQueryResults("whatWasWhere", GetThing(name), TS);
        if(queryResults.size() > 0){
            return queryResults.iterator().next().get("$P");
        }
        else{
            System.out.println("thing " + name + " not found");
            return null;
        }
    }

    // NOTE: using factType will fail => if an object is declared within java, kBase refuses to create it!
    static FactHandle InsertThing(String name) {
        // neither of those work...
        //FactType factType = kBase.getFactType("com.sample.streamBase", "thing");
        /*FactType factType = kBase.getFactType("com.customSyntax", "thing");
        Object factInstance = factType.newInstance();
        factType.set(factInstance, "name", name);
        return kSession.insert(factInstance);*/

        // classic java works:
        return kSession.insert(new com.customSyntax.thing(name));
    }

}
