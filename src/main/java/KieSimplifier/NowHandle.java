package KieSimplifier;

import org.kie.api.definition.type.Role;
import org.kie.api.definition.type.Timestamp;

import java.time.Instant;

@Role(Role.Type.EVENT)
@Timestamp("time")
public class NowHandle implements java.io.Serializable, PointInTimeEvent {
    public long time;

    @Override
    public String toString() {
        return "NowHandle = " + Instant.ofEpochMilli(time);
    }

    public NowHandle(long time) {
        this.time = time;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
