package KieSimplifier;

public class TimespanHelper implements java.io.Serializable{

    private long start, stop;
    private String subject = "";
    private boolean enabled = true;




    @Override
    public String toString() {
        return "TimespanHelper{" +
                "start=" + start +
                ", stop=" + stop +
                ", subject='" + subject + "'" +
                ", " + (isEnabled()? "ENABLED" : "DISABLED") +
                '}';
    }

    public TimespanHelper(long start, long stop) {
        this.start = start;
        this.stop = stop;
    }

    public TimespanHelper(long start, long stop, String subject) {
        this.start = start;
        this.stop = stop;
        this.subject = subject;
    }

    public TimespanHelper(long start, long stop, String subject, boolean enabled) {
        this.start = start;
        this.stop = stop;
        this.subject = subject;
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getStop() {
        return stop;
    }

    public void setStop(long stop) {
        this.stop = stop;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
