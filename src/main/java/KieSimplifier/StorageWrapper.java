package KieSimplifier;

import org.apache.commons.collections4.KeyValue;
import org.javatuples.Pair;
import org.kie.api.builder.ReleaseId;

import java.io.InputStream;
import java.io.OutputStream;
import java.time.Instant;

// plug this into a session wrapper to automatically backup sessions
public abstract class StorageWrapper {

    public abstract OutputStream getOutputStream(ReleaseId releaseId, long time);

    // fetches the most recent file before the given time
    public abstract Pair<InputStream, Long> getInputStream(ReleaseId releaseId, long time);

    public String getLongAsTimeString(long time){
        return Instant.ofEpochMilli(time).toString();
    }

    public long getTimeStringAsLong(String time){
        return Instant.parse(time).getEpochSecond();
    }


}
