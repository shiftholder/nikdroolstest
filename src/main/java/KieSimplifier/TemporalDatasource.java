package KieSimplifier;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.EntryPoint;

/*
        Datasource which can be

 */
public abstract class TemporalDatasource {


    public enum FactInsertionStrategy{
        INSERT, REPLACE, REPLACE_ALL
    }

    public FactInsertionStrategy getFactInsertionStrategy(){
        return FactInsertionStrategy.INSERT;
    }

    // get all data starting at time for duration
    public abstract Iterable<Object> getInRange(long timeFrom, long timeTo);

    // name to be used as entry point
    // can be null to use the default
    public abstract String getName();
}
