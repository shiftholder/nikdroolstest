package KieSimplifier;


import org.drools.core.time.SessionPseudoClock;
import org.kie.api.KieBase;
import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.builder.ReleaseId;
import org.kie.api.io.Resource;
import org.kie.api.marshalling.Marshaller;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieSessionConfiguration;
import org.kie.api.runtime.rule.EntryPoint;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.api.time.SessionClock;
import org.kie.internal.marshalling.MarshallerFactory;
import org.kie.internal.utils.KieHelper;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class SessionWrapper {

//region CONFIG

    // TODO: make configuration class
    private KieSessionConfiguration kieSessionConfiguration;
    private KieBaseConfiguration kieBaseConfiguration;
    private long deltaTime = 10000; // default to 10s = 10000ms

    public boolean isRealtime() {
        return realtime;
    }

    private boolean realtime;

    public boolean isAutoSave() {
        return autoSave;
    }

    private boolean autoSave = true;

    // TODO: auto save strategy selection
    private int autoSaveSteps = 1;


//endregion

//region KIE

    private KieServices kieServices;
    private KieHelper kieHelper;

    public KieSession getKieSession() {
        return kieSession;
    }

    private KieSession kieSession;

    public KieBase getKieBase() {
        return kieBase;
    }

    private KieBase kieBase;

    private ReleaseId releaseId;

//endregion

//region MARSHALLING

    private Marshaller getMarshaller() {
        if(marshaller == null)
            marshaller = MarshallerFactory.newMarshaller(kieBase);
        return marshaller;
    }

    private org.kie.api.marshalling.Marshaller marshaller;

    public void setStorageWrapper(StorageWrapper storageWrapper) {
        this.storageWrapper = storageWrapper;
    }

    private StorageWrapper storageWrapper;

    public void storeSessionNow() throws IOException {
        marshall();
    }

    private void marshall() throws IOException {
        OutputStream outputStream = storageWrapper.getOutputStream(releaseId, lastUpdateTime);
        //kieSessionConfiguration = kSession.getSessionConfiguration();
        getMarshaller().marshall(outputStream, kieSession);
    }

    private void unmarshall(long time) throws IOException, ClassNotFoundException {
        var input = storageWrapper.getInputStream(releaseId, time);
        InputStream inputStream = input.getValue0();
        long fileTime = input.getValue1();
        KieSession newSession = getMarshaller().unmarshall(inputStream, kieSessionConfiguration, null);
        //TODO: see if dispose should come first
        if(newSession == null)
            return;
        if(kieSession != null)
            kieSession.dispose();
        kieSession = newSession;
    }

//endregion

//region TIME MANAGEMENT

    // set time handler object within working memory
    // this will delete existing ones and insert a new one
    private void setSessionTimeHandler(){
        // remove all existing now handles (should only be one)
        for(var nh : kieSession.getFactHandles(object -> object instanceof NowHandle))
            kieSession.delete(nh);
        // add new now handle
        kieSession.insert(new NowHandle(lastUpdateTime));

        // NOTE: it is not possible to update event times!
        // the engine will not update time, even when calling update on the fact!
    }

    private List<TemporalDatasource> datasources = new ArrayList<>();

    public void addTemporalDatasource(TemporalDatasource datasource){
        datasources.add(datasource);
    }

    private long lastUpdateTime;

    // to be called when session starts running
    private void initializeSessionTime(){
        lastUpdateTime = kieSession.getSessionClock().getCurrentTime();
        setSessionTimeHandler();
    }

    // ask all temporal datasources for new data between the session clocks now and the last time this was called
    public void insertAllNewObjects(){
        long time = kieSession.getSessionClock().getCurrentTime();
        for(var datasource : datasources){
            // use datasource name as entry point if it exists, otherwise use default
            EntryPoint entryPoint = datasource.getName() == null ?
                    kieSession : kieSession.getEntryPoint(datasource.getName());
            if(entryPoint == null)
                entryPoint = kieSession;
            switch (datasource.getFactInsertionStrategy()){
                case INSERT:    // insert is the default and behaves purely additive
                    break;
                case REPLACE_ALL:
                    // make sure we have an entry point to remove data from
                    // this prevents removing all data from the session
                    if(entryPoint != kieSession){
                        Collection<FactHandle> deleteThis = entryPoint.getFactHandles();
                        for(FactHandle fh : deleteThis)
                            entryPoint.delete(fh);
                    }
                    break;
            }
            // all strategies will insert the new data
            for(var o : datasource.getInRange(lastUpdateTime, time))
                entryPoint.insert(o);
        }
        lastUpdateTime = time;
        // update now in session
        setSessionTimeHandler();
    }

//endregion


    public SessionWrapper(
            ReleaseId releaseId,
            KieBaseConfiguration kieBaseConfiguration,
            KieSessionConfiguration kieSessionConfiguration,
            List<Resource> resources) {
        this.releaseId = releaseId;
        this.kieSessionConfiguration = kieSessionConfiguration;
        this.kieBaseConfiguration = kieBaseConfiguration;
        kieServices = KieServices.Factory.get();
        kieHelper = new KieHelper();
        kieHelper.setReleaseId(releaseId);
        /*for(String s : resources) {
            Resource r = new Resource()
                    .setSourcePath(s)
                    .setTargetPath("src/Main/resources/KBase/" + );
            kieHelper.addResource(r);
        }*/

        for(var r : resources)
            kieHelper.addResource(r);

        kieBase = kieHelper.build(kieBaseConfiguration);
        kieSession = kieBase.newKieSession(kieSessionConfiguration,null);

        realtime = !(kieSession.getSessionClock() instanceof  SessionPseudoClock);
    }

//region RUN


    // fire all rules and return processing time in ms
    // this does not take care about anything time related
    private long runOnce(){
        long start = System.currentTimeMillis();
        kieSession.fireAllRules();
        return System.currentTimeMillis() - start;
    }

    // run information

    public long getRunTimeTaken() {
        return runTimeTaken;
    }

    private long runTimeTaken;

    public long getRunCounter() {
        return runCounter;
    }

    private long runCounter;


    // run until specified time is reached
    // lambdas can be null
    public void run(long stopAtTime, Function<SessionWrapper, Boolean> beforeRun, Function<SessionWrapper, Boolean> afterRun){
        runTimeTaken = 0;
        runCounter = 0;

        // TODO: proper init
        initializeSessionTime();
        while(kieSession.getSessionClock().getCurrentTime() < stopAtTime){
            // advance timespan
            advanceTime();
            insertAllNewObjects();
            // run
            if(beforeRun != null)
                beforeRun.apply(this);
            runTimeTaken += runOnce();
            runCounter++;
            if(afterRun != null)
                afterRun.apply(this);
            // store snapshot
            if(isAutoSave() && ((runCounter % autoSaveSteps) == 0)){
                try {
                    storeSessionNow();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void advanceTime(){
        if(!isRealtime())
            ((SessionPseudoClock) kieSession.getSessionClock()).advanceTime(deltaTime, TimeUnit.MILLISECONDS);
    }


//endregion

//region DEBUG

    public String getKieInfo(){
        String s =  "#########################[ INFO ]###########################\n\n";
        for (KieSession kieSession : kieBase.getKieSessions()) {
            s += "kieSession =  " + kieSession.getSessionConfiguration().getProperty("name") + "\n";
        }
        for (var p : kieBase.getProcesses()) {
            s += "kBase.getProcesses =  " + p.getId() + "\n";
        }
        // show fact properties
        for (var p : kieBase.getKiePackages()){
            for (var f : p.getFactTypes()){
                for (var ff : f.getFields())
                    s += "package = " + p.getName() + "    " + "fact = " + f.getName() + " property = " + ff.getName() + "\n";
            }
        }
        //System.out.println("sessionclock = " + kSession.getSessionClock().toString());
        return s;
    }

    public String getAllFactsAsString() {
        StringBuilder s = new StringBuilder();
        for (var ep : kieSession.getEntryPoints()){
            s.append(ep.toString()).append(" :\n\n");
            for (Object o : ep.getObjects()) {
                s.append("\t").append(o.toString()).append("\n");
            }
            s.append("\n").append("   fact count = ").append(ep.getFactCount()).append("\n\n");
        }
        return s.toString();
    }


//endregion

}
