package KieSimplifier;

public interface PointInTimeEvent {
    public long getTime();
    // set should not be used when an object is in working memory
    //public void setTime();
}
