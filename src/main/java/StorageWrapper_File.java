import KieSimplifier.StorageWrapper;
import org.apache.poi.ss.usermodel.ComparisonOperator;
import org.javatuples.Pair;
import org.kie.api.builder.ReleaseId;

import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;

public class StorageWrapper_File extends StorageWrapper {

    String folderPath;

    public StorageWrapper_File(String folderPath) throws Exception {
        this.folderPath = folderPath;
        File folder = new File(folderPath);
        if(!folder.mkdirs())
            if(!folder.exists())
                throw new Exception("folder " + folderPath + " could not be created");
    }

    @Override
    public OutputStream getOutputStream(ReleaseId releaseId, long time){
        File newFile = new File(
                folderPath +
                        URLEncoder.encode(releaseId.toString(), StandardCharsets.UTF_8) +
                        "__" +
                        URLEncoder.encode(getLongAsTimeString(time), StandardCharsets.UTF_8) +
                        ".msl"
                );
        try {
            newFile.createNewFile();
            return new FileOutputStream(newFile);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Pair<InputStream, Long> getInputStream(ReleaseId releaseId, long time) {
        File folder = new File(folderPath);
        String releaseIdAsURL = URLEncoder.encode(releaseId.toString(), StandardCharsets.UTF_8);
        FilenameFilter filenameFilter = (dir, name) ->
                name.startsWith( releaseIdAsURL ) &&
                name.endsWith(".msl");
        // find most recent file below given time
        long max = 0;
        File maxFile = null;
        for(File f : Objects.requireNonNull(folder.listFiles(filenameFilter))){
            String fileName = f.getName();
            String fileNameTime = URLDecoder.decode(fileName.substring(fileName.indexOf("__") + 1, fileName.length() - 4), StandardCharsets.UTF_8 );
            long fileTime = getTimeStringAsLong(fileNameTime);
            // find max while ignoring times above given time
            if(fileTime > max && fileTime <= time){
                max = fileTime;
                maxFile = f;
            }
        }
        if(maxFile == null)
            return null;
        try {
            return new Pair<>(new FileInputStream(maxFile), max);
        } catch (FileNotFoundException e) {
            // the file has to exist, so this should never happen
            e.printStackTrace();
            return null;
        }
    }
}
