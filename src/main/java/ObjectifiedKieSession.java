import org.kie.api.runtime.KieSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// converts a kiesession into a list of object mapped by entry points
// this is to freeze a session for debug purposes
public class ObjectifiedKieSession {
    public Map<String, List<Object>> entryPoints = new HashMap<>();

    ObjectifiedKieSession(KieSession kieSession){
        for (var ep : kieSession.getEntryPoints()) {
            List<Object> objects = new ArrayList<>();
            entryPoints.put(ep.toString(), objects);
            objects.addAll(ep.getObjects());
        }
    }
}
