package com.customSyntax;

import KieSimplifier.TimespanHelper;

public class WeatherRuleSetting extends TimespanHelper {

    private float trigger;



    public WeatherRuleSetting(long start, long stop, String subject, float trigger) {
        super(start, stop, subject);
        this.trigger = trigger;
    }

    public float getTrigger() {
        return trigger;
    }

    public void setTrigger(float trigger) {
        this.trigger = trigger;
    }
}
