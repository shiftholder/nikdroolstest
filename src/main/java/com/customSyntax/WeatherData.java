package com.customSyntax;

import KieSimplifier.PointInTimeEvent;
import org.kie.api.definition.type.Role;
import org.kie.api.definition.type.Timestamp;

@Role(Role.Type.EVENT)
@Timestamp("time")
public class WeatherData implements java.io.Serializable, PointInTimeEvent {

    @Override
    public String toString() {
        return "WeatherData{" +
                "time=" + time +
                ", value=" + value +
                '}';
    }

    public WeatherData(long time, float value) {
        this.time = time;
        this.value = value;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public float value;
    public long time;

}
