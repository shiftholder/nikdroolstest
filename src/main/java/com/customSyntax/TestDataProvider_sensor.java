package com.customSyntax;

import KieSimplifier.TemporalDatasource;

import java.util.ArrayList;

public class TestDataProvider_sensor extends TemporalDatasource {

    private static final int substeppingResolution = 1000;
    public ArrayList<Place> places = new ArrayList<>();


    private float makeUpSensorData(long time, Place p){
        // make up data "randomly"
        double d1 = (double)    time % (p.x + 10);
        double d2 = (double)    time % (p.y + 20);
        return (float) (d1 * d2);
    }

    @Override
    public Iterable<Object> getInRange(long timeFrom, long timeTo) {
        timeFrom /= substeppingResolution;
        timeTo /= substeppingResolution;
        ArrayList<Object> ret = new ArrayList<>();
        for(long t = timeFrom; t < timeTo; t++)
            for(Place p : places)
                ret.add(new SensorData(t*1000, (float) makeUpSensorData(t*1000, p), p));
        return ret;
    }

    @Override
    public String getName() {
        return "sensor";
    }

    /*public SensorData Get(long time, long duration, Place p) {
        time /= 1000; // do downstepping to per-second data
        duration /= 1000;
        double result = 0;
        for(long t = time; t < time+duration; t++)
            result += makeUpSensorData(time, p);
        result /= duration;
        return new SensorData(time, (float) result,p);
    }*/
}
