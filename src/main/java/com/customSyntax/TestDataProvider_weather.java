package com.customSyntax;

import KieSimplifier.TemporalDatasource;

import java.util.ArrayList;

public class TestDataProvider_weather extends TemporalDatasource {

    private static final int substeppingResolution = 10000;

    @Override
    public FactInsertionStrategy getFactInsertionStrategy() {
        return FactInsertionStrategy.REPLACE_ALL;
    }

    private float makeUpWeatherData(long time, long timePredict){
        // make up data "randomly"
        long confusion = timePredict - time;
        confusion *= confusion;
        confusion /= 20;
        if(confusion % 2 == 0)
            confusion *= -1;
        confusion = confusion > 10 ? 10 : confusion;
        confusion = confusion < -10 ? -10 : confusion;
        final long sineDuration = 8;
        double actualValue = 10 * Math.sin((double)(timePredict % sineDuration) * 2 * Math.PI / (double)(sineDuration) );
        actualValue += (double) confusion;
        return (float) actualValue;
    }

    @Override
    public Iterable<Object> getInRange(long timeFrom, long timeTo) {
        timeFrom /= substeppingResolution;
        timeTo /= substeppingResolution;
        ArrayList<Object> ret = new ArrayList<>();
        long t = timeTo;
        for(long tp = t; tp < t + 30; tp++)
            ret.add(new WeatherData(tp*substeppingResolution, (float) makeUpWeatherData(t, tp)));
        return ret;
    }

    @Override
    public String getName() {
        return "weather";
    }
}
