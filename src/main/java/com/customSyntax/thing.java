package com.customSyntax;


public class thing implements java.io.Serializable{
    public String name;

    public thing(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
