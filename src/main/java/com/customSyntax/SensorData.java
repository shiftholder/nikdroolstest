package com.customSyntax;

import KieSimplifier.PointInTimeEvent;
import org.kie.api.definition.type.Role;
import org.kie.api.definition.type.Timestamp;

@Role(Role.Type.EVENT)
@Timestamp("time")
//@Expires( value = "30s", policy = Expires.Policy.TIME_SOFT )
// expire is absolutely broken, so don't even bother
public class SensorData implements java.io.Serializable, PointInTimeEvent {

    public long time;
    public float val;
    public Place p;


    public SensorData(long time, float val, Place p) {
        this.time = time;
        this.val = val;
        this.p = p;
    }

    public SensorData(float val, Place p) {
        this.val = val;
        this.p = p;
    }

    @Override
    public String toString() {
        return p.toString() + " @ " + time /*Instant.ofEpochMilli(time).toString()*/ + " = " + val;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public float getVal() {
        return val;
    }

    public void setVal(float val) {
        this.val = val;
    }

    public Place getP() {
        return p;
    }

    public void setP(Place p) {
        this.p = p;
    }
}
