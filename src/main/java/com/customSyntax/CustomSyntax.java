package com.customSyntax;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.function.Function;

public class CustomSyntax {

    public static <T, U extends Comparable<? super U>> T GetMin(Collection<T> collection, Function<? super T, ? extends U> keyExtractor) {
        return Collections.min(collection, Comparator.comparing(keyExtractor));
    }

    public static <T, U extends Comparable<? super U>> T GetMax(Collection<T> collection, Function<? super T, ? extends U> keyExtractor) {
        return Collections.min(collection, Comparator.comparing(keyExtractor));
    }

}
