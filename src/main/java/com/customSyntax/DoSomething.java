package com.customSyntax;

import KieSimplifier.PointInTimeEvent;
import org.kie.api.definition.type.Role;
import org.kie.api.definition.type.Timestamp;

@Role(Role.Type.EVENT)
@Timestamp("time")
public class DoSomething implements java.io.Serializable, PointInTimeEvent {

    public DoSomething(float value, float value2, long time) {
        this.value = value;
        this.value2 = value2;
        this.time = time;
    }

    public float value;

    public float getValue2() {
        return value2;
    }

    public void setValue2(float value2) {
        this.value2 = value2;
    }

    public float value2;
    public long time;

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
