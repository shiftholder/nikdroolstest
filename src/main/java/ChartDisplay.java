import KieSimplifier.NowHandle;
import KieSimplifier.PointInTimeEvent;
import KieSimplifier.TimespanHelper;
import com.customSyntax.DoSomething;
import com.customSyntax.SensorData;
import com.customSyntax.TestDataProvider_weather;
import com.customSyntax.WeatherData;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.knowm.xchart.*;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ChartDisplay {

    private ArrayList<ObjectifiedKieSession> sessions;
    private BubbleChart chart = new BubbleChartBuilder().xAxisTitle("time").yAxisTitle("Y").width(1000).height(800).build();
    JFrame jFrame;

    public ChartDisplay(ArrayList<ObjectifiedKieSession> sessions){
        this.sessions = sessions;

        jFrame = new SwingWrapper<BubbleChart>(chart).displayChart();
        jFrame.pack();
        jFrame.setAlwaysOnTop(true);
        jFrame.setVisible(true);
    }

    public void show(int i){
        if(i < sessions.size()){
            for (Map.Entry<String, List<Object>> ep : sessions.get(i).entryPoints.entrySet()){
                ArrayList<Long> timeValues = new ArrayList<Long>();
                ArrayList<Float> otherValues = new ArrayList<Float>();
                ArrayList<Float> errorValues = new ArrayList<Float>();
                for (Object o : ep.getValue()) {
                    if(o instanceof PointInTimeEvent){
                        timeValues.add(((PointInTimeEvent)o).getTime());
                        if(o instanceof WeatherData) {
                            var w = (WeatherData)o;
                            otherValues.add(w.getValue());
                            errorValues.add(5f);
                        }
                        else if(o instanceof SensorData) {
                            var s = (SensorData) o;
                            otherValues.add(s.getVal());
                            errorValues.add(5f);
                        }
                        else if(o instanceof NowHandle){
                            var n = (NowHandle) o;
                            otherValues.add(0f);
                            errorValues.add(10f);
                        }
                        else if(o instanceof DoSomething){
                            var d = (DoSomething) o;
                            otherValues.add(d.value);
                            errorValues.add(d.value2 + 1f);
                        }
                        else{
                            otherValues.add(1f);
                            errorValues.add(1f);
                        }
                    }
                }
                if(timeValues.size() != 0){
                    if(chart.getSeriesMap().containsKey(ep.getKey())){
                        chart.updateBubbleSeries(ep.getKey(), timeValues, otherValues, errorValues);
                    }
                    else{
                        chart.addSeries(ep.getKey(), timeValues, otherValues, errorValues);
                    }
                }
            }
        }
        jFrame.repaint();
    }

}
